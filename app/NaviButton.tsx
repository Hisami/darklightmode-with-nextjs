"use client"
import { useState, useEffect } from "react";
import { useTheme } from "next-themes";

export default function NaviButton() {
    const [mounted, setMounted] = useState(false);
    // Default to false
    const [darkTheme, setDarkTheme] = useState<boolean>(false);
    const { theme, setTheme } = useTheme();

    const handleToggle = () => {
        setDarkTheme(!darkTheme);
        setTheme(darkTheme ? "light" : "dark")
    }

    useEffect(() => {
        setMounted(true)
        // Set the darkTheme state based on the current theme
        setDarkTheme(theme === "dark");
    }, [theme])

    if (!mounted) {
        return null;
    }
    return (
        <>
            <nav>
                <div className="title">My portfolio</div>
                <div>
                    {theme}
                    <form action="#">
                        <label className="switch">
                            <input type="checkbox"
                                onChange={handleToggle}
                                checked={darkTheme}
                            />
                            <span className="slider"></span>
                        </label>
                    </form>
                </div>
            </nav>
        </>
    )
}